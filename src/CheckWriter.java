/**
 * Created by Dmitry on 7/2/2015.
 */

import java.util.Scanner;

public class CheckWriter {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String number;
        String dollarsNum;
        String dollarsCap;
        String dollars;
        String cents;
        String name;
        String date;
        Double amount;
        String rule;

        System.out.print("Enter the date (format: mm/dd/yyyy): ");
        date = keyboard.nextLine();
        //date = "11/24/2012";
        System.out.print("Enter the payee's name: ");
        name = keyboard.nextLine();
        //name = "John Phillips";
        System.out.print("Enter the amount: $");
        amount = keyboard.nextDouble();
        //amount = 1920.85;
        number = Double.toString(amount);
        String[] money = number.split("\\.");
        dollarsNum = money[0];
        cents = money[1];
        char[] numberArray = dollarsNum.toCharArray();
        dollars = convertEng(numberArray);
        dollarsCap = dollars.substring(0, 1).toUpperCase() + dollars.substring(1);

        System.out.println("\n");

        for (int a = 0; a < 5; a++) {
            System.out.print("|");

            if (a == 0){
                for (int b = 0; b < 90; b++)
                    System.out.print("-");
            }

            if (a == 1){
                for (int b = 0; b < 65; b++)
                    System.out.print(" ");
                    String fullDate = "Date:  " + date;
                    System.out.print(fullDate);
                for (int c = 0; c < 90 - 65 - fullDate.length(); c++)
                    System.out.print(" ");
            }

            if (a == 2){
                String fullName = "   Pay to the Order of:   " + name;
                String fullNumber = "     $" + number + "         ";
                System.out.print(fullName);
                for (int d = 0; d < 90 - fullName.length() - fullNumber.length(); d++)
                    System.out.print(" ");
                System.out.print(fullNumber);
            }

            if (a == 3){
                if (cents.equals("1"))
                    rule = " cent";
                else
                    rule = " cents";
                String fullMoney = "   " + dollarsCap + "and " + cents + rule;
                System.out.print(fullMoney);
                for (int e = 0; e < 90 - fullMoney.length(); e++)
                    System.out.print(" ");
            }

            if (a == 4){
                for (int f = 0; f < 90; f++)
                    System.out.print("-");
            }

            System.out.println("|");
        }


        keyboard.close();
    }


    public static String convertEng(char[] numberArray) {
        String finNameNumber = "";
        int length = numberArray.length;

        if (numberArray[0] == '0' && numberArray.length == 1) {
            return "zero";
        }

        if (numberArray[0] != '0' && numberArray.length == 1) {
            char oneToNine1 = numberArray[length - 1];
            finNameNumber += convertLessTen(oneToNine1);
        }

        if (numberArray.length == 2) {
            char tens1 = numberArray[length - 2];
            char oneToNine2 = numberArray[length - 1];
            finNameNumber += convertLessHundred(tens1, oneToNine2);
        }

        if (numberArray.length == 3) {
            char hundreds3 = numberArray[length - 3];
            char tens3 = numberArray[length - 2];
            char oneToNine3 = numberArray[length - 1];

            finNameNumber += convertLessTen(hundreds3) + "hundred " + convertLessHundred(tens3, oneToNine3);
        }

        if (numberArray.length == 4) {
            char thousands4 = numberArray[length - 4];
            char hundreds4 = numberArray[length - 3];
            char tens4 = numberArray[length - 2];
            char oneToNine4 = numberArray[length - 1];

            finNameNumber += convertLessTen(thousands4) + "thousand " + checkHundreds(hundreds4) + convertLessHundred(tens4, oneToNine4);
        }

        if (numberArray.length == 5) {
            char thousands5_1 = numberArray[length - 5];
            char thousands5_2 = numberArray[length - 4];
            char hundreds5 = numberArray[length - 3];
            char tens5 = numberArray[length - 2];
            char oneToNine5 = numberArray[length - 1];

            finNameNumber += convertLessHundred(thousands5_1, thousands5_2) + "thousand " + checkHundreds(hundreds5) + convertLessHundred(tens5, oneToNine5);
        }

        if (numberArray.length == 6) {
            char thousands6_1 = numberArray[length - 6];
            char thousands6_2 = numberArray[length - 5];
            char thousands6_3 = numberArray[length - 4];
            char hundreds6 = numberArray[length - 3];
            char tens6 = numberArray[length - 2];
            char oneToNine6 = numberArray[length - 1];

            finNameNumber += convertLessTen(thousands6_1) + "hundred " + convertLessHundred(thousands6_2, thousands6_3) + "thousand " + checkHundreds(hundreds6) + convertLessHundred(tens6, oneToNine6);
        }
        return finNameNumber;
    }

    public static String convertLessTen(char numberChar) {
        String name = "";
        switch (numberChar) {
            case '1':
                name = "one ";
                break;
            case '2':
                name = "two ";
                break;
            case '3':
                name = "three ";
                break;
            case '4':
                name = "four ";
                break;
            case '5':
                name = "five ";
                break;
            case '6':
                name = "six ";
                break;
            case '7':
                name = "seven ";
                break;
            case '8':
                name = "eight ";
                break;
            case '9':
                name = "nine ";
                break;
            default:
                name = "";
        }
        return name;
    }

    public static String convertLessTwenty(char numberChar) {
        String name = "";
        switch (numberChar) {
            case '0':
                name = "ten ";
                break;
            case '1':
                name = "eleven ";
                break;
            case '2':
                name = "twelve ";
                break;
            case '3':
                name = "thirteen ";
                break;
            case '4':
                name = "fourteen ";
                break;
            case '5':
                name = "fifteen ";
                break;
            case '6':
                name = "sixteen ";
                break;
            case '7':
                name = "seventeen ";
                break;
            case '8':
                name = "eighteen ";
                break;
            case '9':
                name = "nineteen ";
                break;
            default:
                name = "";
        }
        return name;
    }

    public static String convertLessHundred(char tens, char ones) {
        String name = "";
        switch (tens) {
            case '0':
                name = convertLessTen(ones);
                break;

            case '1':
                if (ones == '0')
                    name = "ten ";
                else
                    name = convertLessTwenty(ones);
                break;

            case '2':
                if (ones == '0')
                    name = "twenty ";
                else
                    name = "twenty-" + convertLessTen(ones);
                break;

            case '3':
                if (ones == '0')
                    name = "thirty ";
                else
                    name = "thirty-" + convertLessTen(ones);
                break;

            case '4':
                if (ones == '0')
                    name = "forty ";
                else
                    name = "forty-" + convertLessTen(ones);
                break;

            case '5':
                if (ones == '0')
                    name = "fifty ";
                else
                    name = "fifty-" + convertLessTen(ones);
                break;

            case '6':
                if (ones == '0')
                    name = "sixty ";
                else
                    name = "sixty-" + convertLessTen(ones);
                break;

            case '7':
                if (ones == '0')
                    name = "seventy ";
                else
                    name = "seventy-" + convertLessTen(ones);
                break;

            case '8':
                if (ones == '0')
                    name = "eighty ";
                else
                    name = "eighty-" + convertLessTen(ones);
                break;

            case '9':
                if (ones == '0')
                    name = "ninety ";
                else
                    name = "ninety-" + convertLessTen(ones);
                break;

            default:
                name = "";
        }
        return name;
    }

    public static String checkHundreds(char numberChar) {
        String name;
        if (numberChar == '0')
            name = "";
        else
            name = convertLessTen(numberChar) + "hundred ";
        return name;
    }

}